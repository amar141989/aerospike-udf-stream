function add_stat_ops(stream)
    return stream :
        aggregate(map{count = 0, total = 0, sumsqs = 0,
                      min = nil, max = nil}, agggregate_stats) :
                  reduce(reduce_stats)
end
