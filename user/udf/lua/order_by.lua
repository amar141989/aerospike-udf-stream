------------------------------------------------------------------------------------------
--  Order By and Limit
------------------------------------------------------------------------------------------
function order_by(stream, u_sort_key, u_order)

    local function map_record(rec, fields)
      info("8. In map_record")
      -- Could add other record bins here as well.
      -- This code shows different data access to record bins
      local result = map()
  
      if fields ~= nil then -- selected fields
      for v in list.iterator(fields) do
        result[v] = rec[v]
      end
      end
  
      if fields == nil then -- all fields
      local names = record.bin_names(rec)
      for i, v in ipairs(names) do
        result[v] = rec[v]
      end
      end
      result["meta_data"] = map()
      result["meta_data"]["digest"] = record.digest(rec)
      result["meta_data"]["generation"] = record.gen(rec)
      result["meta_data"]["key"] = record.key(rec)
      result["meta_data"]["set_name"] = record.setname(rec)
      result["meta_data"]["expiry"] = record.ttl(rec)
      return result
    end
  
    local function compare(x, y)
      info("7. compare")
      return (x < y and -1 ) or (x == y and 0 or 1)
    end
  
    local function compare(x, y, order)
      info("6. compare order")
      if order == "ASC" then
        return x < y
      else -- DESC
        return y < x
      end
    end
  
    local function list_truncate(l, limit)
      info("5. In list_truncate")
      if list.size(l) > limit then
        info("5.1 list.size[%d] > limit[%d]. Trucate it.", list.size(l), limit)
        list.trim(l, limit + 1)
      end
    end
  
    -- insert a rec into a sorted list, return the insertion index for merge sort
    local function insert_sort(sorted_list, rec_map, sort_key, order, start_index)
      info("4. In insert_sort")
      local v = rec_map[sort_key]
      info("4.1 sort_key: %s, order: %s, value: %s", sort_key, order, v)
      if v == nil then
        return 0
      end
  
      len = list.size(sorted_list)
      for i = start_index or 1, len do
        v2 = sorted_list[i][sort_key]
        if compare(v, v2, order) then
          list.insert(sorted_list, i, rec_map)
          return i
        end
      end
  
      list.append(sorted_list, rec_map)
      return len
    end
  
    local function sort_aggregator(sort_key, order, limit)
      info("2. In sort_aggregator")
      -- insert a rec into a sorted list is quite easy
      return function(sorted_list, rec)
        -- convert rec to map
        local rec_map = map_record(rec)
  
        -- apply orderBy
        insert_sort(sorted_list, rec_map, sort_key, order)
  
        -- apply limit
        list_truncate(sorted_list, limit)
  
        return sorted_list
      end
    end
  
    local function sort_reducer(sort_key, order, limit)
        info("3. In sort_reducer")
      return function(sorted_list1, sorted_list2)
        -- apply merge sort
        local start_index;
        for i = 1, list.size(sorted_list2) do
          local rec_map = sorted_list2[i]
          start_index = insert_sort(sorted_list1, rec_map, sort_key, order, start_index)
        end
  
        -- apply limit
        list_truncate(sorted_list1, limit)
        return sorted_list1
      end
    end
  
    -- default order by id ASC, limit 100
    local sort_key = u_sort_key
    local order = u_order
    local limit = 100
    info("1. In order by udf function")
    local aggregator = sort_aggregator(sort_key, order, limit)
    local reducer = sort_reducer(sort_key, order, limit)
    return stream : aggregate(list(), aggregator) : reduce(reducer)
  end