import aerospike

# Define host configuration
config = {
    'hosts': [ ('127.0.0.1', 3000) ]
}
# Establishes a connection to the server
client = aerospike.client(config).connect()
policy = {'socket_timeout': 300}
key = ('test', 'test', 1)
(key_, meta, bins) = client.get(key, policy=policy)

# Do something
print('Record: ', bins)

# Get bins 'report' and 'location'
(key_, meta, bins) = client.select(key, ["name"], policy=policy)

# Do something
print('Record: ', bins)
# Close the connection to the server
client.close()